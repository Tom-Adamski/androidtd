package com.example.td4

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_repo.view.*

class RepoViewHolder(itemView : View, listener: RepoClickListener) : RecyclerView.ViewHolder(itemView) {
    private val name: TextView = itemView.repo_name
    private val price: TextView = itemView.repo_owner_name

    private var url : String = ""

    init{
        itemView.setOnClickListener {
            listener.onRepoClick(url)
        }
    }

    fun display(repo: Repo) {
        name.text = repo.name
        price.text = repo.owner.login

        url = repo.html_url
    }
}