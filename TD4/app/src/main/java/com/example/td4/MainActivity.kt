package com.example.td4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import android.content.Intent
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.Uri


class MainActivity : AppCompatActivity(), RepoClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getAndDisplay("JakeWharton")


        search_button.setOnClickListener {
            val userName = name_input.text.toString()
            getAndDisplay(userName)
        }


    }

    private fun getAndDisplay(userName : String){
        val builder = Retrofit.Builder()
            .baseUrl("https://api.github.com")
            .addConverterFactory(GsonConverterFactory.create())

        val retrofit = builder.build()
        val client = retrofit.create(GitHubClient::class.java)
        val call = client.userRepositories(userName)


        call.enqueue(object : Callback<List<Repo>> {
            override fun onResponse(call: Call<List<Repo>>, response:
            Response<List<Repo>>) {
                val listRepo = response.body()
                if(listRepo != null){
                    displayRepos(listRepo)
                }
            }
            override fun onFailure(call: Call<List<Repo>>, t: Throwable) {
                Toast.makeText(this@MainActivity, "Error...!!!", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun displayRepos(listRepo: List<Repo>){
        myRecyclerView.layoutManager = LinearLayoutManager(this)
        myRecyclerView.adapter = RepoAdapter(listRepo, this)
    }

    override fun onRepoClick(url: String) {
        if (url != "") {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }
    }


}
