package com.example.td4

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class RepoAdapter(private val listRepo: List<Repo>, private val listener: RepoClickListener) :
    RecyclerView.Adapter<RepoViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_repo, parent, false)
        return RepoViewHolder(view, listener)
    }
    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        holder.display(listRepo[position])
    }
    override fun getItemCount() = listRepo.size


}