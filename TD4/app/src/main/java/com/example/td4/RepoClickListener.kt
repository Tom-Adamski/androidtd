package com.example.td4

interface RepoClickListener {
    fun onRepoClick(url : String)
}