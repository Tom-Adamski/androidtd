package com.example.td4

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GitHubClient {


    @GET("users/{username}/repos")
    fun userRepositories(@Path("username") userName: String) : Call<List<Repo>>

}