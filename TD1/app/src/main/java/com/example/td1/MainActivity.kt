package com.example.td1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.td1.arithmetic.BadSyntaxException
import com.example.td1.arithmetic.ExpressionParser
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception


class MainActivity : AppCompatActivity() {

    var operation : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun addToOperation( v : View){

        operation += v.tag.toString()
        display.setText(operation)
    }

    fun calculer( v : View) {
        if(operation != "") {

            try {
                val resultat = ExpressionParser().evaluate(operation)
                if (resultat == Int.MIN_VALUE) {
                    Toast.makeText(applicationContext, "-Infini", Toast.LENGTH_LONG).show()
                }
                else if (resultat ==Int.MAX_VALUE) {
                    Toast.makeText(applicationContext, "Vers l'infini et au-delà !", Toast.LENGTH_LONG).show()
                }
                else {
                    operation = "$resultat"
                }
            }
            catch(e : Exception){
                Toast.makeText(applicationContext, "Syntaxe incorrecte", Toast.LENGTH_LONG).show()
            }
        }
        display.setText(operation)
    }

    fun removeLast( v : View) {
        if(operation.isNotEmpty()) {
            operation = operation.substring(0, operation.length - 1)
        }
        display.setText(operation)
    }

    fun erase(v : View) {
        operation = ""
        display.setText(operation)
    }

}
