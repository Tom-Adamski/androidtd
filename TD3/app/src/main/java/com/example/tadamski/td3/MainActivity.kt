package com.example.tadamski.td3

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val mesJeux = ArrayList<JeuVideo>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        mesJeux.add(JeuVideo("Counter-Strike Global Offensive", 15.0f))
        mesJeux.add(JeuVideo("The Legend of Zelda", 50.0f))
        mesJeux.add(JeuVideo("Red Dead Redemption II", 60.0f))
        mesJeux.add(JeuVideo("Call of Duty Modern Warfare", 40.0f))
        mesJeux.add(JeuVideo("Borderlands 3", 30.0f))
        mesJeux.add(JeuVideo("Minecraft", 10.0f))
        mesJeux.add(JeuVideo("GTA V", 5.0f))
        mesJeux.add(JeuVideo("Overwatch", 25.0f))
        mesJeux.add(JeuVideo("Destiny 2", 0.0f))

        displayList()

        Toast.makeText(this, "Choisissez le type d'affichage dans la barre supérieure", Toast.LENGTH_LONG).show()
    }


    private fun displayList(){
        myRecyclerView.layoutManager = LinearLayoutManager(this)
        myRecyclerView.adapter = VideoGamesListAdapter(mesJeux)
    }


    private fun displayGrid(){
        myRecyclerView.layoutManager = GridLayoutManager(this, 2)
        myRecyclerView.adapter = VideoGamesGridAdapter(mesJeux)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.action_list -> {
                displayList()
            }
            R.id.action_grid -> {
                displayGrid()
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
