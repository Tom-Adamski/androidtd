package com.example.tadamski.td3

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


class VideoGamesGridAdapter (private val listJeux: List<JeuVideo>):
RecyclerView.Adapter<VideoGameViewHolder>()
{


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoGameViewHolder {
        val view =
                LayoutInflater.from(parent.context).inflate(R.layout.item_grid_jeu_video, parent, false)
        return VideoGameViewHolder(view)
    }



    override fun onBindViewHolder(holder: VideoGameViewHolder, position: Int) {
        holder.display(listJeux[position])
    }



    override fun getItemCount() = listJeux.size



}