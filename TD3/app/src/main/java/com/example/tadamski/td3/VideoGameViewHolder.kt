package com.example.tadamski.td3

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_list_jeu_video.view.*

class VideoGameViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val name: TextView = itemView.name
    private val price: TextView = itemView.price

    fun display(jeuVideo: JeuVideo) {
        name.text = jeuVideo.name
        price.text = "${jeuVideo.price.toString()}$"
    }
}