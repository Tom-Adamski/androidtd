package tom.adamski.td2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        toolbar.title = "Login Activity"
        toolbar.setTitleTextColor(resources.getColor(android.R.color.white))
        setSupportActionBar(toolbar)

        button_login.setOnClickListener {

            val name = edit_name.text.toString()
            if(name != "") {

                (applicationContext as NewsListApplication).login = name

                val intent = Intent(this, NewsActivity::class.java)
                startActivity(intent)
                finish()
            }
            else {
                Toast.makeText(this, "Veuillez entrer votre identifiant", Toast.LENGTH_LONG).show()
            }
        }
    }
}
