package tom.adamski.td2

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import kotlinx.android.synthetic.main.activity_login.*

import kotlinx.android.synthetic.main.activity_news.*
import kotlinx.android.synthetic.main.activity_news.toolbar

class NewsActivity : AppCompatActivity() {

    private var login : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        toolbar.title = "News Activity"
        toolbar.setTitleTextColor(resources.getColor(android.R.color.white))
        setSupportActionBar(toolbar)

        login = (applicationContext as NewsListApplication).login

        if(login != null){
            Toast.makeText(this, "Connecté en tant que $login", Toast.LENGTH_SHORT).show()
            username.text = "Bienvenue, $login"
        }


        button_details.setOnClickListener {
            val intent = Intent(this, DetailsActivity::class.java)
            startActivity(intent)
        }

        button_logout.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        button_about.setOnClickListener {
            val url = "http://android.busin.fr/"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }

    }

    override fun onBackPressed() {
        moveTaskToBack(true)
        super.onBackPressed()
    }

    override fun onDestroy() {
        (applicationContext as NewsListApplication).login = null
        super.onDestroy()
    }

}
